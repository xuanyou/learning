## Phoenix webservice concept

One way of using Docker is to reuse existing container images and mount local folders to change their config. This is really good for rapid development (changes to your local folders can be immediately reflected in the container)

However, if you deploy this at scale (e.g. multiple sets of such services), there will always be mutex locks, file sharing issues etc with the local config. To solve the scaling problem, the recommendation is to rebuild a new container image every time there is a code change. This involves using COPY or ADD in the Dockerfile to bring in the new code/config, and using docker-compose build to rebuild the container image before deployment.

This also has the benefit that the container becomes completely known (no snowflake, manual undocumented changes), can be completely tested (no differences between testing and production services) and can be self-healing (any problems, destroy and recreate).
